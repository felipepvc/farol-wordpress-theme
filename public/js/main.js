// HOME > HOME HERO (Carousel)
var HomeHero = {

	init: function() {

		var slides = $('#home-hero .carousel .slides');
		var slide = $('#home-hero .carousel .slides .slide');
		var backwardBtn = $('#home-hero .carousel .backward');
		var forwardBtn = $('#home-hero .carousel .forward');
		var bulletNav = $('#home-hero .carousel .navigation');
		var counter = slide.length;

		if (counter != 1) {
			
			slides.on('init', function(event, slick){
				var element = slide.first();
				HomeHero.loader("start");
				HomeHero.textContent( element, "in" );
				HomeHero.imgContent( element, "show" );
				HomeHero.pagination( "in" );
				HomeHero.btnLink( "in" );
				HomeHero.overlays( "in" );
			});
			
			slides.on('beforeChange', function(event, slick, currentSlide, nextSlide){
				var element = slides.find( ".slide:eq(" + currentSlide + ")" );
				HomeHero.loader("pause");
				HomeHero.textContent( element, "out" );
				HomeHero.imgContent( element, "hide" );
				HomeHero.btnLink( "out" );
				HomeHero.overlays( "out" );
			});
			
			slides.on('afterChange', function(event, slick, currentSlide){
				var element = slides.find( ".slide:eq(" + currentSlide + ")" );
				HomeHero.loader("start");
				HomeHero.textContent( element, "in" );
				HomeHero.imgContent( element, "show" );
				HomeHero.btnLink( "in" );
				HomeHero.overlays( "in" );
			});
			
			slides.on({
				mouseenter: function() {
					HomeHero.loader("stop");
				}, mouseleave: function() {
					HomeHero.loader("continue");
				}
			});
			
			slides.slick({
				fade: true,
				autoplay: true,
				autoplaySpeed: 8000,
				speed: 1000,
				prevArrow: backwardBtn,
				nextArrow: forwardBtn,
				dots: true,
				appendDots: bulletNav,
			});
			
			$('body').keydown(function(key) {

				if (key.which == 37) {
					slides.slick('slickPrev');
				}
				else if (key.which == 39) {
					slides.slick('slickNext');
				}

			});
			
		}

		else {
			var element = slide;
			
			backwardBtn.hide();
			forwardBtn.hide();
		} // In case of less then two slides to display

	},
	
	loader: function( command ){
		var progress = $(".loader .progress");
		
		if ( command == "stop" ){
			progress.velocity("stop", true);
		}
		
		if ( command == "pause" ){
			progress.velocity("stop");
		}
		
		if ( command == "start" ){
			progress.css({"opacity": "0", "width": 0 });
			progress.velocity("transition.fadeIn", { duration: 500, easing: "easeOutQuad" });
			progress.velocity({ width: ["100%", 0] }, { duration: 7000, easing: "easeInOutQuad" });
			progress.velocity("transition.fadeOut", { duration: 500, easing: "easeOutQuad" });
		}
		
		else if ( command == "continue" ){
			if ( progress.css("opacity") != 1 ){
				progress.velocity("transition.fadeIn", { duration: 500, easing: "easeOutQuad" });
			}
			progress.velocity({ width: ["100%"] }, { duration: 7000, easing: "easeInOutQuad" });
			progress.velocity("transition.fadeOut", { duration: 500, easing: "easeOutQuad" });
		}
	},
	
	textContent: function( element, command ){
		
		var capital = element.find(".capital");
		var text = element.find(".text");
		var number = element.find(".number");
		var tag = element.find(".tag");

		
		if ( command == "in" ){
			tag.velocity("transition.fadeIn", { duration: 1100, delay: 250, easing: "easeInOutQuint" });
			capital.velocity("transition.slideLeftBigIn", { duration: 6000, delay: 800, easing: "easeInOutQuint" });
			text.velocity("transition.slideRightBigIn", { duration: 3000, delay: 1300, easing: "easeInOutQuint" });
			
			if( $(window).width() >= 992 ){ 
				number.velocity("transition.slideDownBigIn", { duration: 1000, easing: "easeInOutQuint" });
			} else{
				number.velocity("transition.fadeIn", { duration: 1100, delay: 250, easing: "easeInOutQuint" });
			}
			
		}
		
		else if ( command == "out" ) {
			tag.velocity("stop", true).velocity("reverse", 1200);
			number.velocity("stop", true).velocity("reverse", 1200);
			capital.velocity("stop", true).velocity("reverse", 1200);
			text.velocity("stop", true).velocity("reverse", 1200);
		}

	},
	
	imgContent: function( element, command ){
		
		var content = element.attr('data-src');
		var container = $("#home-hero .imgcontainer");
		var video = element.attr("data-video");
		var homeHero = $("#home-hero");
		var containerSnippet ="<div class='imgcontainer'></div>" 
		var videoSnippet = "<video autoplay muted loop><source type='video/mp4'></video>";
		
		if( command == "hide" ){
			container.velocity("transition.shrinkOut", { duration: 1000, easing: "easeInOutQuint" }).remove();
		}
		else if( command == "show" ){
		
			if( video == "true" ){
				homeHero.prepend( containerSnippet );
				homeHero.find(".imgcontainer").prepend( videoSnippet );
				homeHero.find("video source").attr("src", content);
				homeHero.find(".imgcontainer").velocity("transition.fadeIn", { duration: 400, easing: "easeInOutQuint" });
			}
			else{
				homeHero.prepend( containerSnippet );
				homeHero.find(".imgcontainer").attr('data-src', content);
				homeHero.find(".imgcontainer").lazy({
					beforeLoad: function() {
					},
					afterLoad: function() {
						homeHero.find(".imgcontainer").velocity("transition.shrinkIn", { duration: 6000, easing: "easeInOutQuint" });
					}
				});
			}
		}
	},
	
	btnLink: function( command ){
		
		var btnLink = $("#home-hero .target");
		
		if ( command == "in" ){
			
			if( $(window).width() >= 992 ){ 
				btnLink.velocity("transition.slideUpIn", { duration: 1200, easing: "easeInOutQuint" });
			} else{
				btnLink.velocity("transition.flipXIn", { duration: 1200, easing: "easeInOutQuint" });
			}
		}
		
		else if ( command == "out" ) {
			if( $(window).width() >= 992 ){ 
				btnLink.velocity("stop", true).velocity("reverse", 1200);
			} else{
				btnLink.velocity("stop", true).velocity("reverse", 1200);
			}
		}
	},

	pagination: function( command ){
		
		var btnForward = $("#home-hero .forward");
		var btnBackward = $("#home-hero .backward");
		
		if ( command == "in" ){
			btnForward.velocity("transition.slideLeftBigIn", { duration: 1000, easing: "easeInOutQuint" });
			btnBackward.velocity("transition.slideRightBigIn", { duration: 1000, easing: "easeInOutQuint" });
		}
		
		else if ( command == "out" ) {
			btnForward.velocity("stop", true).velocity("reverse", 600);
			btnBackward.velocity("stop", true).velocity("reverse", 600);
		}
	},
	
	overlays: function( command ){
		
		var overlay1 = $("#home-hero .overlay1");
		var overlay2 = $("#home-hero .overlay2");
		var overlay3 = $("#home-hero .overlay3");
		
		if ( command == "in" ){
			overlay1.velocity("transition.slideLeftBigIn", { duration: 2500, easing: "easeInOutQuint" });
			overlay2.velocity("transition.slideRightBigIn", { duration: 2500, easing: "easeInOutQuint" });
			
			if( $(window).width() >= 992 ){ 
				overlay3.velocity("transition.slideRightBigIn", { duration: 4000, delay: 2000, easing: "easeInOutQuint" });
			} else{
				overlay3.velocity("transition.fadeIn", { duration: 2500, easing: "easeInOutQuint" });
			}
		}
		
		else if ( command == "out" ) {
			overlay1.velocity("stop", true).velocity("reverse", 600);
			overlay2.velocity("stop", true).velocity("reverse", 600);
			overlay3.velocity("stop", true).velocity("reverse", 600);
		}
	}

}

//When the page is loaded
$(document).ready(function() {
	
	svg4everybody(); // To work with SVG icons - As explained in https://fvsch.com/code/svg-icons/how-to/
	HomeHero.init();

	//When scrolling the page
	$(window).bind('scroll', function(e) {

	});

	//When changing the window size
	$(window).resize(function() {

	});

	//When changing the device orientation
	$(window).on("orientationchange", function() {

	});

});