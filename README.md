# Farol Wordpress Theme #

The word's best choice for film creators and agencies to showcase their work, their team and optimize equipment requisitions.


## Table of contents

  - [Sitemap](#sitemap)
  - [Roadmap](#roadmap)
    * [Desirable variations](#desirable-variations)
  - [Installation](#installation)
  - [Contributing](#contributing)


## Sitemap
  - Home
  - Cases
    * The case
  - Directors
    * The director
  - Equipments
    * The equipment
    * Requisition list
  - About
  - Contact
  - 404

## Roadmap

  Details about the project roadmap are organized in tasks on [Asana's project board](https://app.asana.com/0/351263664960034/board).

  The idea is to first build a site like [this reference](http://www.light-house-films.com), beeing carefull about originality, and then work on possible variations to make it more versatile, like a theme pretends to be.

#### Desirable variations
  - Diferent colors
  - Diferent font families

## Installation

  Setup your machine with Node, Grunt-cli and Sass.

#### Install/Update [Node.js](https://nodejs.org/) v4+
Node.js (Node) is an open source development platform for executing JavaScript code server-side. Node.js package ecosystem, npm, is the largest ecosystem of open source libraries in the world.
```sh
$ npm update -g npm
```

#### Install [grunt-cli](https://gruntjs.com/getting-started#installing-the-cli)
This will install Grunt command-line interface that comes with a series of options.
```sh
$ npm install -g grunt-cli
```

#### Install [Sass](http://sass-lang.com/install)
Sass is an extension of CSS that adds power and elegance to the basic language. It allows you to use variables, nested rules, mixins, inline imports, and more, all with a fully CSS-compatible syntax. Sass helps keep large stylesheets well-organized, and get small stylesheets up and running quickly.
```sh
$ sudo gem install sass
$ sudo gem update sass
```


#### Clone this git repository
Now go to the directory where you want the project to be and follow the steps below.
```sh
$ git clone git@bitbucket.org:felipepvc/farol-bootstrap-theme.git
```

#### Install the dependencies
Go one path deeper `$ cd farol-wordpress-theme`, to the repository's root directory where the file package.json is. It will install all the dependencies listed on package.json, including bootstrap-sass.
```sh
$ npm install
```

#### Run Grunt
```sh
$ grunt
```
It will open a new window on your browser, and self update the page everytime you modify and save the html or scss file. It will also compile scss files to css, merge the svg images into a single svg sprite image and other things... All the details about the automations made by grunt are listed in Gruntfile.js.


## Contributing

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create new Pull Request
