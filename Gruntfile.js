module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      mini: {
        options: {                       // Target options
          style: 'compressed',
          noCache: true,
          sourcemap: 'none'
        },
        files: [{
          expand: true,
          cwd: './src/scss/',
          src: ['*.scss'],
          dest: './public/css/',
          ext: '.min.css'
        }]
      },
      print: {
        options: {                       // Target options
          style: 'expanded',
          noCache: true,
          sourcemap: 'none'
        },
        files: [{
          expand: true,
          cwd: 'src/scss/',
          src: ['*.scss'],
          dest: 'public/css/',
          ext: '.css'
        }]
      }
    },
    watch: {
      gruntfile: {
        files: 'Gruntfile.js',
        tasks: ['default'],
      },
      css: {
        files: ['src/scss/**/*'],
        tasks: ['sass', 'postcss'],
      },
      svgs: {
        files: ['src/img/svg/**'],
        tasks: ['svgstore', 'svg2png'],
      },
      readme: {
        files: ['**/*.md', '!**/node_modules/**'],
        tasks: ['md2html'],
      },
    },
    postcss: {
      options: {
        processors: [
          require('autoprefixer')({browsers: 'last 5 versions'}), // add vendor prefixes
          require('cssnano')({ // minify the result
            preset: ['default', {
                discardComments: {
                    removeAll: true,
                },
            }]
          })
        ]
      },
      dist: {
        src: 'public/css/main.min.css'
      }
    },
    browserSync: {
      dev: {
          bsFiles: {
              src : './public/**/*'
          },
          options: {
              watchTask: true,
              server: {
                baseDir: "./public",
                // index: "index.html",
                directory: true
            }
          }
      }
    },
    svgstore: {
      options: {
        cleanup: true,
        cleanupdefs: true,
        includeTitleElement: false,
        preserveDescElement: false,
        convertNameToId: function(name) {
            var dotPos = name.indexOf('.');
            if ( dotPos > -1){
              name = name.substring(0, dotPos);
            }
            return name;
          }
      },
      default : {
        files: {
          './public/img/sprite.svg': ['./src/img/svg/**/*.svg'],
        },
      },
    },
    svg2png: {
      build: {
        files: [{
          cwd: 'src/img/svg/',
          src: ['**/*.svg'],
          dest: 'public/img/'
        }]
      }
    },
    md2html: {
      multiple_files: {
        options: {
          layout: 'public/docs/template.html',
          markedOptions: {
            gfm: true
          }
        },
        files: [{
          expand: true,
          cwd: './',
          src: ['**/*.md', '!**/node_modules/**'],
          dest: 'public/docs',
          ext: '.html'
        }]
      }
    }
  });

  // Load the plugins that provides the tasks.
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-svgstore');
  grunt.loadNpmTasks('grunt-svg2png');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-md2html');

  // Default task(s).
  grunt.registerTask('default', ['md2html', 'sass', 'postcss', 'svgstore', 'svg2png', 'browserSync', 'watch']);

};